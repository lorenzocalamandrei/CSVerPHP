##	ArrayConverterPHP

# Translate in CSV/XML a PHP array.

# Usage:

Require the "project/ArrayConverter.php" and construct the object "new ArrayConverter()" after use the method createCSV(array_to_convert) or createXML(array_to_convert).

Example:
```php
require_once './project/Converter.php';

$array = [
	'first' => [
		'first:first' => 'fiiii',
		'first:second' => 'rrrsst',
	],
	'second' => 'ciao',
];

$conv = new ArrayConverter();

$converted = $conv->createCSV($array);

echo $converted;
```

* author: Michael Zangirolami
* author: Michele Acierno
* author: Lorenzo Calamandrei <nexcal.dev@gmail.com>
