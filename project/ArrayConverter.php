<?php
/**
 * Class Converter
 */

/**
 * Class Converter
 * @author Michael Zangirolami
 * @author Michele Acierno
 * @author Lorenzo Calamandrei <nexcal.dev@gmail.com>
 */
class ArrayConverter
{
	/** @var string $xmlResult */
	private $xmlResult;

	/** @var string $csvResult */
	private $csvResult;

	/**
	 * arrayToCsv
	 * @param $item
	 * @param $key
	 */
	public function arrayToCsv($item, $key)
	{
		if (is_array($item)) {
			array_walk($item, [$this, 'arrayToCsv']);
			$this->csvResult = substr($this->csvResult, 0, -2);
			$this->csvResult .= PHP_EOL;
		} else {
			$this->csvResult .= "{$item}, ";
		}
	}

	/**
	 * createCSV
	 * @param array $array
	 * @param bool $isRecorsive
	 * @return string
	 */
	public function createCSV($array, $isRecorsive = false)
	{
		array_walk($array, [$this, 'arrayToCsv']);
		$this->csvResult = substr($this->csvResult, 0, -2);
		return $this->csvResult;
	}

	/**
	 * arrayToXml
	 * @param $item
	 * @param $key
	 */
	public function arrayToXml($item, $key)
	{
		if (is_array($item)) {
			$this->xmlResult .= "<{$key}>\n";
			array_walk($item, [$this, 'arrayToXml']);
			$this->xmlResult .= "</{$key}>\n";
		} else {
			$this->xmlResult .= "<{$key}>{$item}</{$key}>\n";
		}
	}

	/**
	 * createXML
	 * @param $array
	 * @param string $rootElement
	 * @return string
	 */
	public function createXML($array, $rootElement = '<root/>')
	{

		array_walk($array, [$this, 'arrayToXml']);
		return $this->xmlResult;
	}
}
